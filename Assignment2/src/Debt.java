public class Debt {

	String country;
	long gdp;
	long nationalDebt;
	double debtPercentageGDP;
	int population;
	float debtPerCitizen;

	public Debt(String country, long gdp, long nationalDebt, double debtPercentageGDP, int population, float debtPerCitizen) {
		super();
		this.country = country;
		this.gdp = gdp;
		this.nationalDebt = nationalDebt;
		this.debtPercentageGDP = debtPercentageGDP;
		this.population = population;
		this.debtPerCitizen = debtPerCitizen;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public long getGdp() {
		return gdp;
	}

	public void setGdp(long gdp) {
		this.gdp = gdp;
	}

	public long getNationalDebt() {
		return nationalDebt;
	}

	public void setNationalDebt(long nationalDebt) {
		this.nationalDebt = nationalDebt;
	}

	public double getDebtPercentageGDP() {
		return debtPercentageGDP;
	}

	public void setDebtPercentageGDP(double debtPercentageGDP) {
		this.debtPercentageGDP = debtPercentageGDP;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public float getDebtPerCitizen() {
		return debtPerCitizen;
	}

	public void setDebtPerCitizen(float debtPerCitizen) {
		this.debtPerCitizen = debtPerCitizen;
	}
	
	
	
	

}
