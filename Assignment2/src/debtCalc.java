public abstract class debtCalc {
	// Ireland
	public static Float getIrelandNationalDebt() {
		return new Float(212980444179L);
	}

	public static Integer getIrelandPopulation() {
		return new Integer(4625087);
	}

	public static Float getIrelandDebtPerCitizenNow() {
		return new Float(debtCalc.getIrelandNationalDebt()
				/ debtCalc.getIrelandPopulation());
	}

	// Spain
	public static Float getSpainNationalDebt() {
		return new Float(1094582742945L);
	}

	public static Integer getSpainPopulation() {
		return new Integer(46704308);
	}

	public static Float getSpainDebtPerCitizenNow() {
		return new Float(debtCalc.getSpainNationalDebt()
				/ debtCalc.getSpainPopulation());
	}

	// Finland
	public static Float getFinlandNationalDebt() {
		return new Float(130946802110L);
	}

	public static Integer getFinlandPopulation() {
		return new Integer(5471674);
	}

	public static Float getFinlandDebtPerCitizenNow() {
		return new Float(debtCalc.getFinlandNationalDebt()
				/ debtCalc.getFinlandPopulation());
	}

	// Germany
	public static Float getGermanyNationalDebt() {
		return new Float(2210892604469L);
	}

	public static Integer getGermanyPopulation() {
		return new Integer(83751602);
	}

	public static Float getGermanyDebtPerCitizenNow() {
		return new Float(debtCalc.getGermanyNationalDebt()
				/ debtCalc.getGermanyPopulation());
	}

	// Greece
	public static Float getGreeceNationalDebt() {
		return new Float(373685723778L);
	}

	public static Integer getGreecePopulation() {
		return new Integer(10812508);
	}

	public static Float getGreeceDebtPerCitizenNow() {
		return new Float(debtCalc.getGreeceNationalDebt()
				/ debtCalc.getGreecePopulation());
	}

}
