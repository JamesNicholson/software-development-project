import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class DebtTabbedGUI extends JFrame implements ActionListener {

	JMenuBar menuBar = new JMenuBar();
	JMenu menu = new JMenu("Menu");
	JMenuItem exit = new JMenuItem("Exit");

	private JButton addCountryButton = new JButton("Submit");
	private JButton cancelButton = new JButton("Cancel");

	private JButton updateCountryButton = new JButton("Update");
	private JButton readCountryButton = new JButton("Read Country");

	private JButton deleteCountryButton = new JButton("Delete");

	private JLabel countryNameLabel = new JLabel("Country: ");
	private JLabel GDPLabel = new JLabel("GDP: ");
	private JLabel nationalDebtLabel = new JLabel("National Debt: ");
	private JLabel debtPercentageGDPLabel = new JLabel(
			"Debt Percentage of GDP: ");
	private JLabel populationLabel = new JLabel("Population: ");
	private JLabel debtPerCitizenLabel = new JLabel("Debt Per Citizen: ");

	private JLabel readCountryLabel = new JLabel(
			"Enter the country you wish to update: ");
	private JLabel updateCountryNameLabel = new JLabel("Country: ");
	private JLabel updateGDPLabel = new JLabel("GDP: ");
	private JLabel updateNationalDebtLabel = new JLabel("National Debt: ");
	private JLabel updateDebtPercentageGDPLabel = new JLabel(
			"Debt Percentage of GDP: ");
	private JLabel updatePopulationLabel = new JLabel("Population: ");
	private JLabel updateDebtPerCitizenLabel = new JLabel("Debt Per Citizen: ");

	private JLabel deleteCountryLabel = new JLabel(
			"Enter the name of the country you want to delete: ");

	private JTextField countryNameTextField = new JTextField();
	private JTextField nationalDebtTextField = new JTextField();
	private JTextField debtPercentageGDPTextField = new JTextField();
	private JTextField populationTextField = new JTextField();
	private JTextField debtPerCitizenTextField = new JTextField();
	private JTextField GDPTextField = new JTextField();

	private JTextField readCountryTextField = new JTextField();
	private JTextField updateCountryNameTextField = new JTextField();
	private JTextField updateNationalDebtTextField = new JTextField();
	private JTextField updateDebtPercentageGDPTextField = new JTextField();
	private JTextField updatePopulationTextField = new JTextField();
	private JTextField updateDebtPerCitizenTextField = new JTextField();
	private JTextField updateGDPTextField = new JTextField();

	private JTextField deleteCountryTextField = new JTextField(12);

	private DebtFactory theDebtFactory;

	public DebtTabbedGUI() throws HeadlessException {

		// super(title);
		// aPanel = setupPanel();

		// New Frame
		JFrame frame = new JFrame("Tabbed Pane Frame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JTabbedPane tab = new JTabbedPane();
		JPanel viewDebtPanel = new JPanel();
		JPanel addCountryPanel = new JPanel(new GridLayout(10, 2));
		JPanel updateCountryPanel = new JPanel(new GridLayout(10, 2));
		JPanel deleteCountryPanel = new JPanel(new GridBagLayout());

		// MenuBar
		frame.setJMenuBar(menuBar);
		menuBar.add(menu);
		menu.add(exit);
		exit.addActionListener(this);

		// Panels
		tab.addTab("View Debt", viewDebtPanel);
		tab.addTab("Add Country With Debt", addCountryPanel);
		tab.addTab("Update Country", updateCountryPanel);
		tab.addTab("Delete Country", deleteCountryPanel);

		frame.add(tab);

		// View Debt Tab Components
		DebtTable table = new DebtTable();
		viewDebtPanel.add(table);
		JScrollPane someTable = new JScrollPane(table);
		viewDebtPanel.add(someTable);

		// Add Country Tab Components
		// JLabels & JTextFields
		addCountryPanel.add(countryNameLabel);
		addCountryPanel.add(countryNameTextField);
		addCountryPanel.add(GDPLabel);
		addCountryPanel.add(GDPTextField);
		addCountryPanel.add(nationalDebtLabel);
		addCountryPanel.add(nationalDebtTextField);
		addCountryPanel.add(debtPercentageGDPLabel);
		addCountryPanel.add(debtPercentageGDPTextField);
		addCountryPanel.add(populationLabel);
		addCountryPanel.add(populationTextField);
		addCountryPanel.add(debtPerCitizenLabel);
		addCountryPanel.add(debtPerCitizenTextField);

		addCountryPanel.add(addCountryButton);
		addCountryButton.addActionListener(this);

		addCountryPanel.add(cancelButton);
		cancelButton.addActionListener(this);

		// Update Country Tab Components
		updateCountryPanel.add(readCountryLabel);
		updateCountryPanel.add(readCountryTextField);
		updateCountryPanel.add(updateCountryNameLabel);
		updateCountryPanel.add(updateCountryNameTextField);
		updateCountryPanel.add(updateGDPLabel);
		updateCountryPanel.add(updateGDPTextField);
		updateCountryPanel.add(updateNationalDebtLabel);
		updateCountryPanel.add(updateNationalDebtTextField);
		updateCountryPanel.add(updateDebtPercentageGDPLabel);
		updateCountryPanel.add(updateDebtPercentageGDPTextField);
		updateCountryPanel.add(updatePopulationLabel);
		updateCountryPanel.add(updatePopulationTextField);
		updateCountryPanel.add(updateDebtPerCitizenLabel);
		updateCountryPanel.add(updateDebtPerCitizenTextField);

		readCountryTextField.setEditable(true);
		updateCountryNameTextField.setEditable(false);
		updateGDPTextField.setEditable(false);
		updateNationalDebtTextField.setEditable(false);
		updateDebtPercentageGDPTextField.setEditable(false);
		updatePopulationTextField.setEditable(false);
		updateDebtPerCitizenTextField.setEditable(false);

		updateCountryPanel.add(updateCountryButton);
		updateCountryButton.addActionListener(this);

		updateCountryPanel.add(readCountryButton);
		readCountryButton.addActionListener(this);

		// Delete Country Panel Components
		deleteCountryPanel.add(deleteCountryLabel);
		deleteCountryPanel.add(deleteCountryTextField);
		deleteCountryPanel.add(deleteCountryButton);
		deleteCountryButton.addActionListener(this);

		theDebtFactory = DebtFactory.getInstance();
		// Frame Final Touches
		pack();
		frame.setSize(850, 400);
		frame.setVisible(true);

		// NEED:
		// 1. Create Country and its debt
		// 2. Read a country's debt details
		// 3. Update a country's details
		// 4. Delete a country's details
		// 5. Exit application

		// return panel1;
		// making the panel have two rows and one column

		// JPanel panel = new JPanel(new GridLayout(2, 1));

		//
		// JPanel somePanel = new JPanel();
		// panel1.add(new JScrollPane(someTable));
		//
		// add(new JScrollPane(someTable));
		//
		// // add tabbedPane to bottom
		//
		// tabbedPane = new JTabbedPane();
		// tabbedPane.addTab("DebtTable", table);
		// tabbedPane.addTab("Create Country Debt Details", new JLabel("Country
		// Name: "));
		// add(tabbedPane);
		//
		// return JPanel;

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(addCountryButton)) {
			try {
				System.out.println("add country button works");
				String countryName = countryNameTextField.getText();
				Long gdp = Long.parseLong(GDPTextField.getText());
				Long nationalDebt = Long.parseLong(nationalDebtTextField
						.getText());
				Double debtPercentageGDP = Double
						.parseDouble(debtPercentageGDPTextField.getText());
				Integer population = Integer.parseInt(populationTextField
						.getText());
				Float debtPerCitizen = Float.parseFloat(debtPerCitizenTextField
						.getText());
				theDebtFactory.createDebt(countryName, gdp, nationalDebt,
						debtPercentageGDP, population, debtPerCitizen);
				JOptionPane.showMessageDialog(new JFrame(), "Debt created.");
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
		if (e.getSource().equals(cancelButton)) {
			try {
				System.out.println("cancel button clicked");
				countryNameTextField.setText("");
				GDPTextField.setText("");
				nationalDebtTextField.setText("");
				debtPercentageGDPTextField.setText("");
				populationTextField.setText("");
				debtPerCitizenTextField.setText("");
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
		if (e.getSource().equals(updateCountryButton)) {
			try {
				System.out.println("update country button clicked");
				// readCountryTextField.setText("");
				updateCountryNameTextField.setText("");
				updateGDPTextField.setText("");
				updateNationalDebtTextField.setText("");
				updateDebtPercentageGDPTextField.setText("");
				updatePopulationTextField.setText("");
				updateDebtPerCitizenTextField.setText("");

				updateCountryNameTextField.setEditable(false);
				updateGDPTextField.setEditable(false);
				updateNationalDebtTextField.setEditable(false);
				updateDebtPercentageGDPTextField.setEditable(false);
				updatePopulationTextField.setEditable(false);
				updateDebtPerCitizenTextField.setEditable(false);
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
		if (e.getSource().equals(readCountryButton)) {
			try {

				readCountryTextField.setEditable(true);
				updateCountryNameTextField.setEditable(false);
				updateGDPTextField.setEditable(true);
				updateNationalDebtTextField.setEditable(true);
				updateDebtPercentageGDPTextField.setEditable(true);
				updatePopulationTextField.setEditable(true);
				updateDebtPerCitizenTextField.setEditable(true);

				System.out.println("read country button works");
				String countryName = countryNameTextField.getText();
				Debt debt = theDebtFactory.readDebt(countryName);
				JOptionPane.showMessageDialog(new JFrame(),
						"Country Details:\n Country Name:" + debt.getCountry()
								+ "GDP:" + debt.getGdp() + "National Debt: "
								+ debt.getNationalDebt() + "Debt % of GDP: "
								+ debt.getDebtPercentageGDP() + "Population: "
								+ debt.getPopulation() + "Debt per citizen: "
								+ debt.getDebtPerCitizen());

				// String gdp = Long.toString(debt.getGdp());
				// GDPTextField.setText(gdp);
				// String =

			} catch (Exception error) {
				error.printStackTrace();
			}
		}
		if (e.getSource().equals(deleteCountryButton)) {
			try {
				System.out.println("delete country button clicked");
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
		if (e.getSource().equals(exit)) {
			try {
				System.out.println("exit button clicked");
				System.out.println("application closed");
				System.exit(0);
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
	}
}
