import java.util.HashMap;

public class DebtFactory {

	private static DebtFactory theInstance = null;
	private HashMap<String, Debt> theListOfDebt = new HashMap();

	private DebtFactory() {
		System.out.println("Constructor for factory has been called");
	}

	public static DebtFactory getInstance() {
		if (theInstance == null) {
			theInstance = new DebtFactory();
			return theInstance;
		} else {
			System.out.println("The factory does already exist");
			return theInstance;
		}
	}

	public void createDebt(String country, long gdp, long nationalDebt,
			double debtPercentageGDP, int population, float debtPerCitizen) {
		System.out.println("Creating country debt details for: " + country);
		Debt debtRef = new Debt(country, gdp, nationalDebt, debtPercentageGDP,
				population, debtPerCitizen);
		theListOfDebt.put(country, debtRef);

	}

	public Debt readDebt(String countryDebt) {
		return theListOfDebt.get(countryDebt);
	}

}
