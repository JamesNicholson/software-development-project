import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class DebtTable extends JPanel {

	JButton printButton = new JButton("Print to file");

	public DebtTable() {
		super(new GridLayout(1, 0));

		JTable table = new JTable(new MyDebtTable());
		table.setPreferredScrollableViewportSize(new Dimension(800, 80));
		table.setFillsViewportHeight(true);

		// Create the scroll pane and add the table to it.
		JScrollPane scrollPane = new JScrollPane(table);

		// Add the scroll pane to this panel.
		add(scrollPane);
		// add(printButton);

	}

	class MyDebtTable extends AbstractTableModel {
		private String[] columnNames = { "Country", "National Debt",
				"Debt % of GDP", "Population", "Debt Per Citizen(2016)", "GDP" };
		private Object[][] data = {
				{ "Ireland", debtCalc.getIrelandNationalDebt(), new Double(104.06),
						debtCalc.getIrelandPopulation(),
						debtCalc.getIrelandDebtPerCitizenNow(),
						new Float(206410986571L) },
				{ "Spain", debtCalc.getSpainNationalDebt(), new Double(100.96),
						debtCalc.getSpainPopulation(),
						debtCalc.getSpainDebtPerCitizenNow(),
						new Float(1084125482180L) },
				{ "Finaland", debtCalc.getFinlandNationalDebt(), new Double(62.41),
						debtCalc.getFinlandPopulation(),
						debtCalc.getFinlandDebtPerCitizenNow(),
						new Float(209830056936L) },
				{ "Germany", debtCalc.getGermanyNationalDebt(), new Double(75.23),
						debtCalc.getGermanyPopulation(),
						debtCalc.getGermanyDebtPerCitizenNow(),
						new Float(2938727723435L) },
				{ "Greece", debtCalc.getGreeceNationalDebt(), new Double(215.54),
						debtCalc.getGreecePopulation(),
						debtCalc.getGreeceDebtPerCitizenNow(),
						new Float(173374977020L) },

		};

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return data.length;
		}

		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Object getValueAt(int row, int col) {
			return data[row][col];
		}

		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		public boolean isCellEditable(int row, int col) {
			// Note that the data/cell address is constant,
			// no matter where the cell appears onscreen.
			if (col > 2) {
				return false;
			} else {
				return true;
			}
		}

		public void setValueAt(Object value, int row, int col) {
			data[row][col] = value;
			fireTableCellUpdated(row, col);

		}
	}
}
